# Importing Bio for opening and manipulating fasta files
from Bio import SeqIO as io
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna


# Finds repeats of arbitrary length inside given sequence.
def find_repeats(x, data):
    new = {}
    for n, i in enumerate(data):
        a = data[n:n + x]
        if data[n + x + 1:n + x + 3] == 'GG':
            if a in new.keys():
                new[a] += 1
            else:
                new[a] = 1
    return new


# Highest speed when searching for highest key:value pair within dictionary
def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    return k[v.index(max(v))]


# Main function for running script
def main():
    try:
        welcome_message = '''
################################################
    Welcome to CRISPR-suicide-sites-finder!!
################################################
'''
        print(welcome_message)
        path_to_fasta = str(input('Input path to fasta file: '))
        kmer = int(input('Enter k-mer length: '))
    except:
        print('Wrong input..')
    else:
        for seq_record in io.parse(path_to_fasta, 'fasta'):
            one = Seq(str(seq_record.seq), generic_dna)
            two = str(one.reverse_complement())
            repeats = find_repeats(kmer, str(seq_record.seq + two))
            max_key = keywithmaxval(repeats)
            print('\n' + str(seq_record.id) + '\n', max_key, repeats[max_key])


if __name__ == '__main__':
    main()
