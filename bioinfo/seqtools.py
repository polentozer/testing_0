def open_fasta(in_file):
    seq = {}
    with open(in_file, 'r') as x:
        data = [line.strip() for line in x.readlines()[::-1]]

    for n, x in enumerate(data):
        if x[0] == '>':
            seq[x] = ''
        elif data[n + 1][0] != '>':
            data[n + 1] += data[n]
    else:
        for n, x in enumerate(data):
            for key in seq:
                if key == data[n]:
                    seq[key] += data[n - 1]
        else:
            return seq


def overlap(data, k):
    over_list = []
    for name1, seq1 in data.items():
        for name2, seq2 in data.items():
            if seq1[-k:] == seq2[:k] and name1 != name2:
                over_list.append([name1[1:], name2[1:]])
    else:
        return over_list
