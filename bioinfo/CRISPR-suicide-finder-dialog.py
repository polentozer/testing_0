#! /usr/bin/python


# Importing Bio for opening and manipulating fasta files
from Bio import SeqIO as io
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from heapq import nlargest
from tkinter import filedialog


# Finds repeats of arbitrary length inside given sequence.
def find_repeats(x, data):
    new = {}
    for n, i in enumerate(data):
        a = data[n:n + x]
        if data[n + x + 1:n + x + 3] == 'GG':
            if a in new.keys():
                new[a] += 1
            else:
                new[a] = 1
    return new


# Highest speed when searching for highest key:value pair within dictionary
def keywithmaxval(d):
    v = list(d.values())
    k = list(d.keys())
    return k[v.index(max(v))]


# Output to <name>.txt
def file_output(name, l, d):
    seq_id = str(name) + '.txt'
    with open(seq_id, 'a+') as fout:
        for i in l:
            fout.write(i + ': ' + str(d[i]) + '\n')


# Summons a little popup to notify you that script is done
def popup(title, infos):
    import gi
    gi.require_version('Gtk', '3.0')
    from gi.repository import Gtk
    dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.INFO,
                               Gtk.ButtonsType.OK, title)
    dialog.format_secondary_text(infos)
    dialog.run()


# Main function for running script
def main():
    fname = filedialog.askopenfilename()
    try:
        for rec in io.parse(fname, 'fasta'):
            s1 = Seq(str(rec.seq), generic_dna)
            s2 = str(s1.reverse_complement())
            repeats = find_repeats(20, str(s1 + s2))
            top = nlargest(1000, repeats, key=repeats.get)
            file_output(rec.id, top, repeats)
    except:
        popup('Warning!', 'Oops, something went wrong.. :(')
    else:
        done = '''
        Script has finished executing.
        Results should be in <sequence-name>.txt
        '''
        popup('Info', done)


if __name__ == '__main__':
    main()
