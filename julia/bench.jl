function fib(c, d)
    a = 1
    b = 1
    for i in 3:c
        a, b = b, d*a + b
    end
    return b
end

# fib(n, k) = n < 2 ? n : fib(n - 1) + k*fib(n - 2)
# fib(100, 4)

function test()
    z = 0
    while z < 1000001
        fib(30, 3)
        z += 1
    end
end

@time test()
