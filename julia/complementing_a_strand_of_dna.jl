data = "AAAACCCGGT"

# Solution 1
function test(c::Char)
    c == 'A' && return 'T'
    c == 'C' && return 'G'
    c == 'G' && return 'C'
    c == 'T' && return 'A'
end

print(reverse(map(test, data)))


# Solution 2
print('\n')

d = Dict("A" => 'T', "C" => 'G', "G" => 'C', "T" => 'A')

print(reverse(replace(data, r"[ACGT]{1}", x -> d[x])))
