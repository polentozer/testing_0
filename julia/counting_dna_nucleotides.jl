data = "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"

nt = Dict('A' => 0, 'C' => 0, 'G' => 0, 'T' => 0)

for n in data
    nt[n] += 1
end

print(nt['A'], ' ', nt['C'], ' ', nt['G'], ' ', nt['T'])
