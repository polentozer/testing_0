# Importing pandas, numpy and plotly
import pandas as pd
import numpy as np
import plotly.offline as py
import plotly.graph_objs as go

# Reading in a document
df = pd.read_csv('../raw_data/fluo-scan_blank-1c.csv')
df_1 = np.array(df)

# Making list of x, y and z values
x_data = []
y = []
z = []

for i in df:
    x_data.append(i)

for i in df.loc[:, 'w']:  # Change value of 'w'.
    y.append(i)

x = x_data[1:]  # x-coordnates

for i in df_1:
    # print(row[1:])
    z.append(i[1:])


# Ploting in 2D heatmap
data = [go.Heatmap(z=z, x=x, y=y, colorscale='Viridis')]
py.plot(data, filename='plots/2d')
