# Importing pandas, numpy and plotly
import pandas as pd
import numpy as np
import plotly.offline as py
import plotly.graph_objs as go


# Reading document
df = pd.read_csv('raw_data/absorbance-scan2.csv')
df_a = df.ix[:, '200nm':]
df_a1 = ((df_a + df_a.shift(-11)) / 2)[:11]  # averaging every 2 rows

# print(df)
df_1 = np.array(df_a1)

# Making list of x values
x = []

for i in df:
    x.append(i)

x_data = x[1:]  # x-coordnates

# Creating traces and putting them in data list
data = []
i_names = ['0.0 g/L', '0.2 g/L', '0.4 g/L', '0.6 g/L', '0.8 g/L', '1.0 g/L',
           '1.2 g/L', '1.4 g/L', '1.6 g/L', '1.8 g/L', '2.0 g/L']

for n, i in enumerate(df_1):
    # trace = go.Scatter(x=x_data, y=i[1:], mode='lines', name=i[0])
    trace = go.Scatter(x=x_data, y=i, mode='lines', name=i_names[n])
    data.append(trace)

layout = dict(title='Folic acid - absorbance scan',
              xaxis=dict(title='Wavelength'),
              yaxis=dict(title='RAU'))

fig = dict(data=data, layout=layout)

# Ploting traces
# py.plot(data, filename='plots/absorbance-scan2-average-lineplot.html')
py.plot(fig, filename='plots/absorbance-scan2-average-lineplot.html')
