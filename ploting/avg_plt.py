# Mandatory imports
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt


# Needed for data munching with time averaging
def get_sec(time_str):
    h, m, s = time_str.split(':')
    return int(h) * 3600.0 + int(m) * 60.0 + float(s)


# Plotting data
def dataPlotting(data_frame, fname, t):
    # Values for plotting
    times = [time.strftime('%H:%M:%S', time.gmtime(secs))
             for secs in list(data_frame.TIME_S)]
    values = list(data_frame.pH_VALUE)
    temp = dict(zip(times, values))
    t_value = temp[t]

    # Plotting data (names, styles, ticks,...)
    plt.style.use('ggplot')
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot_date(times, values, '-', linewidth=2)
    ax1.plot_date(t, t_value, 'x', markersize=10)
    ax1.tick_params(axis='x')
    ax1.tick_params(axis='y')
    ax1.spines['bottom']
    ax1.spines['top']
    ax1.spines['left']
    ax1.spines['right']
    ax1.set_title('Mixing Time')
    ax1.set_xlabel('Time')
    ax1.set_ylabel('pH')
    ax1.set_ylim(6, 9)
    # ax1.set_xlim(times[0], times[-1])
    start, end = ax1.get_xlim()
    # ax1.xaxis.set_ticks(np.arange(start, end, 0.0005))
    plt.setp(ax1.xaxis.get_ticklabels(), rotation=45)

    # Saving plot in separate file
    plt.savefig(fname, bbox_inches='tight')


# Munching data
def dataMunching(fname):
    # Data reading and data munching
    df1 = pd.read_csv(fname)
    # df1 = df[100:]
    df_time = df1.iloc[:, 0]
    tt = np.array(df_time)

    for n, t in enumerate(tt):
        tt[n] = float(get_sec(t))

    del df1['TIME.MS']
    df1 = df1.reset_index(drop=True)
    df1.loc[:, 'TIME_S'] = list(tt)
    data = df1.groupby(np.arange(len(df1)) // 5).mean()

    return data


def main():
    # Opening file with data filenames
    with open('../data/ph_logs_small/ph_logs_start.txt', 'r') as fin:
        # data_names = [x.strip() for x in fin.readlines()]
        data_names = []
        start_points = []
        for x in fin.readlines():
            dn, sp = x.split(',')
            data_names.append(dn)
            start_points.append(sp[:-1])

    # Lopping throug data files
    for file_name, start in zip(data_names, start_points):
        # For progress tracking
        print(file_name, start)

        # File names handling
        a = '../data/ph_logs/' + file_name
        b = '../data/ph_plots/' + file_name[:-3] + 'png'
        c = '../data/ph_logs_small/' + file_name

        # Munching
        data = dataMunching(a)

        # Crunching
        dataPlotting(data, b, start)

        # Write averaged data to separate file
        data.to_csv(c)


if __name__ == '__main__':
    main()
