import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

# plt.style.use('seaborn-dark-palette')
# plt.style.use('seaborn-paper')
# plt.style.use('seaborn-pastel')
# plt.style.use('seaborn-poster')
plt.style.use('bmh')
# plt.style.use('dark_background')

df = pd.read_csv('../raw_data/2017_03_15-B2-K5927-CHON.CSV',
                 sep='\;', engine='python', parse_dates=[1, 2], header=None)


axis_date = [str(x) for x in df[0][3:-1]]
axis_ages = [float(x.replace(',', '.')) for x in df[1][3:-1]]
axis_acid = [float(x.replace(',', '.')) for x in df[2][3:-1]]
axis_afom = [float(x.replace(',', '.')) for x in df[3][3:-1]]
axis_base = [float(x.replace(',', '.')) for x in df[4][3:-1]]
axis_suta = [float(x.replace(',', '.')) for x in df[8][3:-1]]
axis_gasf = [float(x.replace(',', '.')) for x in df[10][3:-1]]
axis_jtmp = [float(x.replace(',', '.')) for x in df[11][3:-1]]
axis_ph__ = [float(x.replace(',', '.')) for x in df[13][3:-1]]
axis_po2_ = [float(x.replace(',', '.')) for x in df[14][3:-1]]
axis_stir = [float(x.replace(',', '.')) for x in df[15][3:-1]]


fig = plt.figure(1, (23, 9), dpi=100)
host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

par1 = host.twinx()
par2 = host.twinx()
par3 = host.twinx()
par4 = host.twinx()
par5 = host.twinx()
par6 = host.twinx()
par7 = host.twinx()

offset = 60
new_fixed_axis = par2.get_grid_helper().new_fixed_axis
par2.axis['right'] = new_fixed_axis(loc='right', axes=par2,
                                    offset=(offset * 1, 0))
par2.axis['right'].toggle(all=True)
par3.axis['right'] = new_fixed_axis(loc='right', axes=par3,
                                    offset=(offset * 2, 0))
par3.axis['right'].toggle(all=True)
par4.axis['right'] = new_fixed_axis(loc='right', axes=par4,
                                    offset=(offset * 3, 0))
par4.axis['right'].toggle(all=True)
par5.axis['right'] = new_fixed_axis(loc='right', axes=par5,
                                    offset=(offset * 4, 0))
par5.axis['right'].toggle(all=True)
par6.axis['right'] = new_fixed_axis(loc='right', axes=par6,
                                    offset=(offset * 5, 0))
par6.axis['right'].toggle(all=True)
par7.axis['right'] = new_fixed_axis(loc='right', axes=par7,
                                    offset=(offset * 6, 0))
par7.axis['right'].toggle(all=True)


host.set_xlabel('Time [h]')
host.set_ylabel('pH')
par1.set_ylabel('Temperature')
par2.set_ylabel('RPM')
par3.set_ylabel('AntiFoam')
par4.set_ylabel('GasFlow')
par5.set_ylabel('BASE')
par6.set_ylabel('pO2')
par7.set_ylabel('FEED')

p1, = host.plot(axis_ages, axis_ph__, label='pH')
p2, = par1.plot(axis_ages, axis_jtmp, label='Temperature')
p3, = par2.plot(axis_ages, axis_stir, label='RPM')
p4, = par3.plot(axis_ages, axis_afom, label='AntiFoam')
p5, = par4.plot(axis_ages, axis_gasf, label='GasFlow')
p6, = par5.plot(axis_ages, axis_base, label='BASE')
p7, = par6.plot(axis_ages, axis_po2_, label='pO2')
p8, = par7.plot(axis_ages, axis_suta, label='FEED')

host.set_xlim(min(axis_ages), max(axis_ages))
host.set_ylim(5, 10)
par1.set_ylim(30, 40)
par2.set_ylim(950, 1200)
par3.set_ylim(0, 50)
par4.set_ylim(0.000, 0.050)
par5.set_ylim(0, 1000)
par6.set_ylim(0, 500)
par7.set_ylim(0, 2500)

# host.legend()

host.axis['left'].label.set_color(p1.get_color())
par1.axis['right'].label.set_color(p2.get_color())
par2.axis['right'].label.set_color(p3.get_color())
par3.axis['right'].label.set_color(p4.get_color())
par4.axis['right'].label.set_color(p5.get_color())
par5.axis['right'].label.set_color(p6.get_color())
par6.axis['right'].label.set_color(p7.get_color())
par7.axis['right'].label.set_color(p8.get_color())

plt.draw()
plt.savefig('plots/testing.png')
