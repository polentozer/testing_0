import pandas as pd
# import matplotlib as mpl
# mpl.use('TkAgg')
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

# plt.style.use('bmh')
plt.style.use('seaborn-deep')


data = pd.read_csv('../data/mut_2017-05-05.csv', sep=';')

labels = [int(x) for x in data.iloc[:, 0]]
kill = [float(x) for x in data.iloc[:, 3]]
cfu = [int(x) for x in data.iloc[:, 1]]
mut = [float(x)*100 for x in data.iloc[:, 4]]

# fig = plt.figure()
# ax1 = fig.add_subplot(111)
# ax1.plot(labels, cfu, '-', linewidth=2)
# ax1.plot(labels, mut, '-', linewidth=2)
# ax1.tick_params(axis='x')
# ax1.tick_params(axis='y')
# ax1.set_title('Mutagenesis')
# ax1.set_xlabel('UV Duration [sec]')
# ax1.set_ylabel('Kill Rate')
# # ax1.set_yscale('log')
# fig = plt.show()

host = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)
par1 = host.twinx()

host.set_xlim(0, 300)
host.set_ylim(10, 1e8)

# host.set_title('Mutagenesis')
host.set_xlabel('UV Duration [sec]')
host.set_ylabel('CFU')
# host.set_ylabel('Kill Rate [%]')
par1.set_ylabel('Mutation Rate [%]')

p1, = host.plot(labels, cfu, label='CFU', marker='.')
# p1, = host.plot(labels, kill, label='Kill Rate')
p2, = par1.plot(labels, mut, label='Mutation Rate', marker='.')

par1.set_ylim(0, 0.35)

host.legend()

host.set_yscale('log')
host.axis['left'].label.set_color(p1.get_color())
par1.axis['right'].label.set_color(p2.get_color())

plt.draw()
plt.show()
