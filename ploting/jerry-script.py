import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as aa

plt.style.use('bmh')

df = pd.read_csv('../raw_data/2017_03_15-B2-K5927-CHON.CSV', sep='\;',
                 engine='python', parse_dates=[1, 2], header=None)


def ploting(df):
    fig = plt.figure(1, (23, 9), dpi=100)
    host = host_subplot(111, axes_class=aa.Axes)
    plt.subplots_adjust(right=0.75)
    offset = 60
    p_num = len(df.columns)
    parameters = list(df.loc[0])
    axs_list = [list(df[i][3:-1]) for i in range(1, p_num)]
    axs = dict(zip(parameters[1:], axs_list))
    # print(axs)
    pars = ['par' + str(i) for i in range(1, p_num)]
    # print(pars)
    pars2 = {}
    for a in pars:
        pars2[a] = host.twinx()

    # for y in pars:
    #     print(y)


ploting(df)
# print(df.columns)
