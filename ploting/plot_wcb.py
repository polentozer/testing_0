import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

fname = '../Experiments/wcb_prep1.csv'

origin = pd.read_csv(fname)

labels = np.array(origin['CANDIDATE'])
yields = np.array(origin['YIELD'])
errors = np.array(origin['ERROR'])

plt.style.use('seaborn-deep')
plt.figure()
plt.errorbar(labels, yields, yerr=errors, fmt='.', ecolor='C1')
plt.xlim(0, 50)
plt.ylim(0, 400)
plt.title('LU17007 colonies in microwell plate')
plt.xlabel('Candidate')
plt.ylabel('Fusaricidins yield [mg/L]')
plt.draw()
plt.show()
