from random import randint


board = []
gg = 0
turn = 1
game = True
go = True

for x in range(5):
    board.append(["O"] * 5)


def print_board(board):
    for row in board:
        print(" ".join(row))


def random_row(board):
    return randint(0, len(board) - 1)


def random_col(board):
    return randint(0, len(board[0]) - 1)


ship_row = random_row(board)
ship_col = random_col(board)


def check_input(x, g):
    if g < 3:
        try:
            val = int(x)
        except ValueError:
            g += 1
            return g
        else:
            return int(x)


print("Let's play Battleship!")

if game is True and gg < 3:
    while turn <= 5:
        print('\nTurn', turn)
        print_board(board)

        # guess_col = input("Guess col: ")
        # guess_row = input("Guess row: ")

        #
        # try:
        #     val = int(g_col) or int(g_row)
        # except ValueError:
        #     print("That's not a valid option!")
        #     break
        if game is True and gg < 3:
            guess_col = input('Guess col: ')
            check_input(guess_col, gg)
        else:
            break

        if game is True and gg < 3:
            guess_row = input('Guess row: ')
            check_input(guess_row, gg)
        else:
            break

        if game is True:
            if guess_row == ship_row and guess_col == ship_col:
                print("\nCongratulations! You sunk my battleship!")
                break
            else:
                if (guess_row < 0 or guess_row > 4) or (guess_col < 0 or
                                                        guess_col > 4):
                    print("\nOops, that's not even in the ocean.")
                elif(board[guess_row][guess_col] == "X"):
                    print("\nYou guessed that one already.")
                else:
                    print("\nYou missed!")
                    board[guess_row][guess_col] = "X"
                    if turn == 5:
                        print('\nGame Over')
                        break
                    turn += 1
        else:
            print('\nGame over!')
            break
else:
    print('\nGame over!')
