# Variables:
pyg = 'ay'
run = True

# While loop:
while run is True:
    original = input('Enter a word:')
    if len(original) > 0 and original.isalpha():
        x = original.lower()
        first = x[0]
        new_word = x[1:] + first + pyg
        print(new_word)
        run = False
    else:
        print('Wrong!')
        run = True
