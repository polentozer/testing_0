# import quandl
import pandas as pd


# Api key lahko direkt vpises, samo tak je prikladno, ker ga lahko samo v
# enem fajlu spreminjas.
api_key = open('apis/quandl.api', 'r').read()
# df = quandl.get('FMAC/HPI_AK', authtoken=api_key)
# print(df.head())

us_states_ugly = pd.read_html('https: // simple.wikipedia.org / wiki /\
                              List_of_U.S._states')

# This is a list --> it contains dataframes --> i need dataframe
# 1 (or 0 in python).
# print(us_states_ugly)

# This is a dataframe 1 from the list of dataframes above.
# print(us_states_ugly[0])

# First column of the first dataframe.
print(us_states_ugly[0][0])

# with this for function i get all the states :)
# for i in us_states_ugly[0][0][1:]:
#     query = "FMAC/HPI_" + str(i)
#     df = quandl.get(query, authtoken = api_key)
