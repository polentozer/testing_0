# Importing pandas and numpy
import pandas as pd
import numpy as np


# Setting variables
raw = pd.read_csv('raw_data/test111.csv')
df_a = np.array(raw)
df_b = []

for row in df_a:
    df_b.append(row[1:])


# Defining function that will fix integers in our dataset
def f_or_i(x):
    y = 0
    integers = []
    integers_fixed = []
    # fixed_list = [integers, integers_fixed]
    for row in x:
        for n, element in enumerate(row):
            if element == int(element):
                y += 1
                pos0 = list(row).index(element) - 1
                pos1 = list(row).index(element) + 1
                row[n] = (list(row)[pos0] + list(row)[pos1]) / 2
                integers.append(element)
                e_fixed = (list(row)[pos0] + list(row)[pos1]) / 2
                integers_fixed.append(e_fixed)
    print(
        'Number of fixed integers: {}.\nIntegers:\n{}\n\Fixed:\n{}'.format
        (y, integers, integers_fixed))
    return x


# Outputing data:
# f_or_i(df_b)
df_c = f_or_i(df_b)
df_d = pd.DataFrame(df_c)
df_d.to_csv('raw_data/test-edited.csv')
