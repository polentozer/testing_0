import pandas as pd

df1 = pd.DataFrame({'Year': [2001, 2002, 2003, 2004],
                    'Int_rate': [2, 3, 2, 2],
                    'US_GDP_Thousands': [50, 55, 65, 55]})

df2 = pd.DataFrame({'Year': [2001, 2003, 2004, 2005],
                    'Unemployment': [7, 8, 9, 6],
                    'Low_tier_HPI': [50, 52, 50, 53]})

# Zmanjkajo ti podatki, ki se v indexu(dobis z on=***) ne prekrivajo.
# merged = pd.merge(df1, df2, on = 'Year')

# Default option
# merged = pd.merge(df1, df2, on = 'Year', how = 'inner')

# Gre v levo, torej df1 je osnova in zdruzi samo tiste, ki se nahajajo v levi
# to vidis po podatkih, ki ostanejo in tistih, ki se izgubijo.
# merged = pd.merge(df1, df2, on = 'Year', how = 'left')

# Isto kot zgoraj, le da v desno, torej je desna osnova.
# merged = pd.merge(df1, df2, on = 'Year', how = 'right')

# 'Outer' je funkcija, ki jo iscem, ki zdruzi vse skupaj.
merged = pd.merge(df1, df2, on='Year', how='outer')

# Nastavim katera kolona se bo stela za index.
merged.set_index('Year', inplace=True)

print(merged)
