def reverse(text):  # which is equal to --> string[::-1]
    new = ''
    x = len(text)
    while x > 0:
        new += (text[x - 1])
        x -= 1
    return new


x = 'New York City'

print('method 1', reverse(x))
print('method 2', x[::-1])
