import pandas as pd

# df = pd.read_csv('LBMA-GOLD.csv')
# print(df.head())
#
# df.set_index('Date', inplace=True)
#
# # df.to_csv('newcsv.csv')
#
# df = pd.read_csv('newcsv.csv')
# print(df.head())
#
# df = pd.read_csv('newcsv.csv', index_col=0)
# print(df.head())
#
# df.columns = ['USD(AM)', 'USD(PM)', 'GBP(AM)', 'GBP(PM)', 'EURO(AM)',
#   'EURO(PM)']
# print(df.head())
#
# df.to_csv('newcsv3.csv')
# df.to_csv('newcsv4.csv', header=False)
#
df = pd.read_csv('newcsv4.csv', names=['USD(AM)', 'USD(PM)', 'GBP(AM)',
                                       'GBP(PM)', 'EURO(AM)', 'EURO(PM)'])
print(df.head())

df.rename(columns={'GBP(AM)': 'Great'}, inplace=True)
print(df.head())
