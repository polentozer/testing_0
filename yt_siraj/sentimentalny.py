import tweepy
from textblob import TextBlob

# Accessing my api keys and tokens
my_cred = open('../apis/twitter.api', 'r')
cred = []

for line in my_cred.readlines()[1::2]:
    cred.append(line[:-1])
else:
    my_cred.close()

consumer_key = cred[0]
consumer_secret = cred[1]
access_token = cred[2]
access_token_secret = cred[3]

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

# Actual code for searching stuff
search = input('Topic of interest: ')
num = int(input('Number of results: '))

public_tweets = api.search(search, count=num)


for tweet in public_tweets:
    print(tweet.text)
    analysis = TextBlob(tweet.text)
    print(analysis.sentiment)
