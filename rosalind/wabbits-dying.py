######
# 1.) The population begins in the first month with a pair of newborn rabbits.
# 2.) Rabbits reach reproductive age after one month.
# 3.) In any given month, every rabbit of reproductive age mates with another
# rabbit of reproductive age.
# 4.) Exactly one month after two rabbits mate, they produce one male and one
# female rabbit.
# 5.) All rabbits die and stop reproducing after given number of generations.
######
# import timeit


def fibonacci(n, k, m):
    f = {}

    for i in range(1, n + 1):
        if i == 1 or i == 2:
            f[i] = 1
        elif i <= m:
            f[i] = (k * f[i - 2]) + f[i - 1]
        elif i <= m + 1:
            f[i] = (k * f[i - 2]) + f[i - 1] - 1
        else:
            f[i] = (k * f[i - 2]) + f[i - 1] - f[i - m - 1]

    print('\nAfter {} generations number of rabbit pairs is {}.'.format
          (n, f[n]))


print('To calculate number of rabbits after X generations, please input:')
n = int(input('Number of generations: '))
k = int(input('Number of offspring per pair: '))
m = int(input('How many generations do rabbits survive? '))


# For benchmarking reverse comment and uncomment [import timeit] at the top.
fibonacci(n, k, m)
# print(timeit.timeit('fibonacci(5000,100,15)', setup='from __main__ import \
# fibonacci', number=1000))
