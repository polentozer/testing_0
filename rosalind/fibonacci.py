def fib_immortal(raw):
    n, k = raw
    f = {}

    for i in range(1, n + 1):
        if i == 1 or i == 2:
            f[i] = 1
        else:
            f[i] = (k * f[i - 2]) + f[i - 1]

    if f[n] < 1e6:
        print('\nAfter {} generations number of rabbit pairs is {}\n'.format
              (n, f[n]))
    else:
        print('\nAfter {} generations number of rabbit pairs is {:.2E}\n'
              .format(n, f[n]))


def fib_mortal(raw):
    n, k, m = raw
    f = {}

    for i in range(1, n + 1):
        if i == 1 or i == 2:
            f[i] = 1
        elif i <= m:
            f[i] = (k * f[i - 2]) + f[i - 1]
        elif i <= m + 1:
            f[i] = (k * f[i - 2]) + f[i - 1] - 1
        else:
            f[i] = (k * f[i - 2]) + f[i - 1] - f[i - m - 1]

    if f[n] < 1e6:
        print('\nAfter {} generations number of rabbit pairs is {}\n'.format
              (n, f[n]))
    else:
        print('\nAfter {} generations number of rabbit pairs is {:.2E}\n'
              .format(n, f[n]))


def immortal_input():
    n = int(input('Number of generations: '))
    k = float(input('Number of offspring per pair: '))
    return(n, k)


def mortal_input():
    n = int(input('Number of generations: '))
    k = float(input('Number of offspring per pair: '))
    m = int(input('How many generations do rabbits survive? '))
    return(n, k, m)


def mortality():
    dying = str(input('Are rabbits mortal? (Y/N): '))
    if dying.lower() == 'y' or dying.lower() == 'yes':
        return True
    elif dying.lower() == 'n' or dying.lower() == 'no':
        return False
    else:
        print('Wrong input..')


def main():
    wrong = 'Wrong input..'
    print('\n### Wellcome to (dying) wabbits! ###\n')

    try:
        raw = mortality()
    except:
        print(wrong)
    else:
        if raw is True:
            print('\nTo calculate number of rabbits after X generations, '
                  'please input:')
            try:
                fib_mortal(mortal_input())
            except:
                print(wrong)
                main()
        elif raw is False:
            print('\nTo calculate number of rabbits after X generations, '
                  'please input:')
            try:
                fib_immortal(immortal_input())
            except:
                print(wrong)
                main()
        else:
            main()


if __name__ == '__main__':
    main()
