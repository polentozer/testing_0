######
# 1.) The population begins in the first month with a pair of newborn rabbits.
# 2.) Rabbits reach reproductive age after one month.
# 3.) In any given month, every rabbit of reproductive age mates with another
# rabbit of reproductive age.
# 4.) Exactly one month after two rabbits mate, they produce one male and one
# female rabbit.
# 5.) Rabbits never die or stop reproducing.
######

print('To calculate number of rabbits after X generations, please input:')
n = int(input('Number of generations: '))
k = int(input('Number of offspring per pair: '))


def fibonacci(n, k):
    f = {}

    for i in range(1, n + 1):
        if i == 1 or i == 2:
            f[i] = 1
        else:
            f[i] = (k * f[i - 2]) + f[i - 1]

    print('\nAfter {} generations number of rabbit pairs is {}.'.format
          (n, f[n]))


fibonacci(n, k)
