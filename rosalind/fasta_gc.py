# For calculating which sequence has the biggest gc value in given fasta file.

# Change path to data!
with open('path/to/data.txt', 'r') as sample:
    data = [line[:-1] for line in sample.readlines()[::-1]]


def get_data(read_fasta_list):
    seq = {}

    for n, x in enumerate(data):
        if x[0] == '>':
            seq[x] = ''
        elif data[n + 1][0] != '>':
            data[n + 1] += data[n]

    for n, x in enumerate(data):
        for key in seq:
            if key == data[n]:
                seq[key] += data[n - 1]
    return seq


def gc_content(sequence):
    gc = 0

    for c in sequence.lower():
        if c == 'c' or c == 'g':
            gc += 1

    return ((gc / len(sequence)) * 100)


def gc_compare(sequence_dictionary):
    seq_gc = {}

    for key in sequence_dictionary:
        seq_gc[key] = gc_content(sequence_dictionary[key])

    return seq_gc


gc_values = (gc_compare(get_data(data)))
max_value_sequence = max(gc_values, key=gc_values.get)

print(max_value_sequence[1:])
print(gc_values[max_value_sequence])
