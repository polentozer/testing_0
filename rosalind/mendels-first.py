from itertools import product
from numpy import array


def raw_in():
    a = float(input('Number of dominant homozygotes (XX): '))
    b = float(input('Number of heterozygotes (Xy): '))
    c = float(input('Number of recesive homozygotes (yy): '))
    return a, b, c


def probability(raw):
    zygs = 'XX', 'Xy', 'yy'
    d = dict(zip(zygs, raw))
    l = list(product(zygs, repeat=2))
    ps = sum(d.values())
    values = [(d[key[0]] * (d[key[0]] - 1)) / (ps * (ps - 1)) if
              key[0] == key[1] else (d[key[0]] * (d[key[1]])) /
              (ps * (ps - 1)) for key in l]
    probs = [1.0 if k[0] == 'XX' or k[1] == 'XX' else 0.75 if
             k[0] == k[1] == 'Xy' else 0.0 if k[0] == k[1] == 'yy' else
             0.5 for k in l]
    # d2 = dict(zip(l1, values))  # probabilities for mating
    # d3 = dict(zip(l1, probs))  # probabilities for dominant allele
    return sum(array(values) * array(probs))


def output(prob):
    print('\nProbability that two randomly selected mating organisms will '
          'produce an individual possessing a dominant allele is: {0:.2f}%'
          .format(prob * 100))


def main():
    try:
        raw = raw_in()
    except:
        print('Wrong input..')
        main()
    else:
        output(probability(raw))


if __name__ == '__main__':
    main()
