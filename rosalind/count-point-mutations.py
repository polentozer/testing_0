with open('data/rosalind_hamm.txt', 'r') as sample:
    data = [line[:-1] for line in sample]

count = 0
for n, base in enumerate(data[0]):
    if base != data[1][n]:
        count += 1
else:
    print(count)
