#! /usr/bin/python

import serial
import time
import datetime

UNO = '/dev/ttyACM0'
ser = serial.Serial(UNO, 115200)

# milliseconds
repeatTime = 1000


def writeData(value):
    # Get the current data
    today = datetime.date.today()

    # Open log file 2012-6-23.log and append
    with open(str(today) + '.log', 'ab') as f:

        # Write our integer value to our log
        f.write(value)

        # Add a newline so we can retrieve the data easily later, could use
        # spaces too.
        f.write('\n')


# Timer to see when we started
timer = time.time()

while True:
    # sleep for 250 milliseconds
    time.sleep(0.25)
    if time.time() - timer > repeatTime:
        # If the current time is greater than the repeat time, send our 'get'
        # command again
        serial.write("t\n")
        # start the timer over
        timer = time.time()

    # if we have \r\n in there, we will have two characters, we want more!
    if ser.inWaiting() > 2:
        value = serial.read()
        # Arduino will return DATA\r\n when println is used
        value = value.strip('\r\n')

        try:
            # This is likely where you will get 'duff' data and catch it.
            value = int(value)
            writeData(value)  # Write the data to a log file

        except:
            # Try it again!
            serial.write("t\n")
